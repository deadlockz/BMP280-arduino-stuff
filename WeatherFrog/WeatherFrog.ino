#include <MsTimer2.h>
#include <Wire.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#include <Adafruit_GFX.h>
#include "Adafruit_SSD1306.h"

#define MAX_POWER  4250.0
#define MIN_POWER  3600.0
#define TEMPMOD    -3

int hours    = 0;
int minutes  = 13;
int seconds  = 0;

/**
 * connect the I2C SSD1306 128x32 Display and the BMP280 Sensor
 * to SDA -> A4  and SCL -> A5
 */

struct Midways {
  float _basic;
  float _val[100];
  int _nxt;

  Midways(float initval) {
    _nxt = 0;
    _basic = initval;
    for (int i=0; i<100; ++i) { 
      _val[i] = _basic;
    }
  }

  void add(float val) {
    _val[_nxt] = val;
    _nxt++;
    if (_nxt == 100) {
      _nxt = 0;
      /*  modify basic with 10% of difference from middle.
       *  This makes it easier to equalize pressure changes on a longer timespan and
       *  force detecting altitude changes in a short timespan.
       */
      _basic += 0.1 * (midget() - _basic);
    }
  }

  float midget() {
    float mid = 0;
    for (int i=0; i<100; ++i) mid += _val[i];
    
    return mid/100.0;
  }
};

Midways * mee;
float alt;
char stri[21];

int mode = 0;

float hpa_last[4] = {0,0,0,0};

// battery
byte maxLength = 28;
int  vcc;

Adafruit_BMP280 bmp; // I2C
Adafruit_SSD1306 display(4); // 4 is a dummy


static const uint8_t PROGMEM sun[] = {
B00000100,
B10111000,
B01000101,
B10000010,
B10000010,
B01000101,
B10111000,
B00100100
};

static const uint8_t PROGMEM rain[] = {
B01100100,
B10011010,
B10000010,
B10000001,
B11111111,
B00000000,
B01001001,
B10010010
};

static const uint8_t PROGMEM cloud[] = {
B00000000,
B01100100,
B10011010,
B10000010,
B10000001,
B11111111,
B00000000,
B00000000
};

static const uint8_t PROGMEM pic[6][160] = {{
B00000000,B00000000,
B00000011,B00110000,
B00000100,B11001100,
B00011000,B00000010,
B00100000,B00000010,
B00111111,B11111110,
B00000000,B00000000,
B00100100,B10001000,
B01000000,B10010000,
B01001001,B00100000
},{
B00000000,B00000000,
B00000000,B00000000,
B00000000,B00000000,
B00110000,B00000000,
B01001111,B10011000,
B10000010,B01100110,
B11111100,B00000001,
B00010000,B00000001,
B00011111,B11111111,
B00000000,B00000000
},{
B00000000,B00000000,
B00000000,B00000000,
B00000011,B00110000,
B00000100,B11001100,
B00011000,B00000010,
B00100000,B00000010,
B00111111,B11111110,
B00000000,B00000000,
B00000000,B00000000,
B00000000,B00000000
},{
B00000000,B00000000,
B00100000,B10000000,
B10010001,B00000000,
B01011010,B00000000,
B00100101,B10011000,
B11000010,B01100110,
B01001100,B00000001,
B10110000,B00000001,
B00011111,B11111111,
B00000000,B00000000
},{
B00010000,B10000000,
B00001001,B11000000,
B00000110,B00110001,
B00011000,B00001010,
B00010000,B00000100,
B01110000,B00000100,
B00011000,B00001000,
B00100110,B00110110,
B01001001,B11000001,
B00010000,B10000000
},{
B00010000,B10000000,
B01001001,B11000000,
B00100110,B00110001,
B00011000,B00001010,
B00010001,B01000100,
B01110000,B00000111,
B00011001,B11001000,
B00100100,B00010110,
B01001011,B11100001,
B00010000,B10010000
}};

void balken(int x, int y, int maxVal, int minVal, int val) {
  if (val <= minVal) {
    val = 0;
  } else if(val >= maxVal) {
    val = maxVal;
  } else {
    val = maxLength * (float)(val-minVal)/((float)(maxVal-minVal));
  }
  display.drawRect(x,   y,                 8, maxLength+2, WHITE);
  display.fillRect(x+2, y+1+maxLength-val, 4, val, WHITE);    
}

int readVcc() {
  int mv;
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(10); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
  mv = ADCL; 
  mv |= ADCH<<8; 
  mv = 1126400L / mv;
  return mv;
}

void drawit(float * a, int siz, int id, float bas) {
  int x = 85; // >= size
  int y = display.height()/2; // +/- 8 would be good
  if (id < 0) id = siz-1;
  
  for (int i=0; i<x; ++i) {
    display.drawPixel(x-i, y - 4*(a[id] - bas), WHITE);
    
    id = id - 1;
    if (id < 0) id = siz-1;
  }
}

void tick() {
  seconds++;
  if (seconds%5 == 0) mode++;
  if (mode > 3) mode = 0;
  
  if (seconds == 60) {
    minutes++; seconds = 0;
  }
  if (minutes == 60) {
    hours++;
    minutes = 0;
    for (int k=3; k>0; --k) hpa_last[k] = hpa_last[k-1];
  }
  if (hours == 24) {
    hours = 0;
  }
}

byte wetter(int x, int y) {
  if ( (hpa_last[0] - hpa_last[1]) > 1.0 ) {
    if ( (hpa_last[1] - hpa_last[2]) > 1.0 ) {
      if ( (hpa_last[0] - hpa_last[3]) > 4.0 ) {
        if(x>=0) display.drawBitmap(x, y, sun, 8,8, WHITE);
        display.print("sun+breezy");
        return 5;
      }
      if(x>=0) display.drawBitmap(x, y, sun, 8,8, WHITE);
      display.print("++ sun");
      return 4;
    } else {
      if(x>=0) display.drawBitmap(x, y-1, sun, 8,8, WHITE);
      if(x>=0) display.drawBitmap(x+7, y+1, cloud, 8,8, WHITE);
      display.print("+ sunny");
      return 3;
    }
  }
    
  if ( (hpa_last[0] - hpa_last[1]) < -1.0 ) {
    if ( (hpa_last[1] - hpa_last[2]) < -1.0 ) {
      if ( (hpa_last[0] - hpa_last[3]) < -4.0 ) {
        if(x>=0) display.drawBitmap(x, y, rain, 8,8, WHITE);
        display.print("rain+storm");
        return 0;
      }
      if(x>=0) display.drawBitmap(x,    y, cloud, 8,8, WHITE);
      if(x>=0) display.drawBitmap(x+10, y, cloud, 8,8, WHITE);
      display.print("++ clouds");
      return 1;
    } else {
      if(x>=0) display.drawBitmap(x, y, cloud, 8,8, WHITE);
      display.print("+ cloudy");
      return 2;
    }
  }

  if (hpa_last[0] < 1009) {
    if(x>=0) display.drawBitmap(x, y, rain, 8,8, WHITE);
    display.print("still rain");
    return 0;
    
  } else if (hpa_last[0] < 1013) {
    if(x>=0) display.drawBitmap(x, y, cloud, 8,8, WHITE);
    display.print("still cloud");
    return 1;
    
  } else if (hpa_last[0] < 1014.5) {
    if(x>=0) display.drawBitmap(x,    y,  sun, 8,8, WHITE);
    if(x>=0) display.drawBitmap(x+13, y, rain, 8,8, WHITE);
    display.print("unsettled");
    return 3;
    
  } else if (hpa_last[0] < 1016) {
    if(x>=0) display.drawBitmap(x,   y-1, sun, 8,8, WHITE);
    if(x>=0) display.drawBitmap(x+7, y+1, cloud, 8,8, WHITE);
    display.print("still sunny");
    return 4;
    
  } else {
    if(x>=0) display.drawBitmap(x, y, sun, 8,8, WHITE);
    display.print("still sun");
    return 5;  
  }
}




void setup() {
  // initialize with the I2C addr 0x3C (for the 128x32)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextSize(1);
  display.setTextColor(WHITE);

  if (!bmp.begin()) {  
    // Could not find a valid BMP280 sensor, check wiring!
    while (1);
  }
  delay(100);
  mee = new Midways(bmp.readAltitude(1013.25));
  
  hpa_last[0] = bmp.readPressure()/100;
  hpa_last[1] = bmp.readPressure()/100;
  hpa_last[2] = bmp.readPressure()/100;
  hpa_last[3] = bmp.readPressure()/100;

  for (byte i=0; i<6; ++i) {
    display.clearDisplay();
    display.drawBitmap(56, 11, pic[i], 16,10, WHITE);
    display.display();
    delay(1000);
  }

  MsTimer2::set(1000, tick);
  MsTimer2::start();
}





void loop() {
  delay(200);
  hpa_last[0] = bmp.readPressure()/100;
  mee->add(bmp.readAltitude(1013.25));
  alt = mee->midget();

  display.clearDisplay();
  display.setCursor(0,0);

  if (mode == 0) {
    display.setTextSize(2);
    
    if (hours<10) display.print('0');
    display.print(hours);
    display.print(':');
    if (minutes<10) display.print('0');
    display.print(minutes);
    display.setTextSize(1);
    display.setCursor(62,0);
    if (seconds<10) display.print('0');
    display.print(seconds);

    display.print(' ');
    dtostrf(alt - mee->_basic, 5, 1, stri);
    display.print(stri);
    display.print('m');
    
    display.setCursor(0,16);
    wetter(85, 13);

    display.setCursor(0,25);
    dtostrf(bmp.readPressure()/100, 6, 1, stri);
    display.print(stri);
    display.print(" hPa");
    
    display.setCursor(85,25);
    display.print((int) bmp.readTemperature() + TEMPMOD);
    display.print(" C");
    
    display.drawLine(121,0,124,0, WHITE);
    balken(119,1,MAX_POWER,MIN_POWER,readVcc());
    
  } else if (mode == 1) {
    
    display.setTextSize(3);
    
    if (hours<10) display.print('0');
    display.print(hours);
    display.print(':');
    if (minutes<10) display.print('0');
    display.print(minutes);
    display.setTextSize(2);
    display.print(' ');
    if (seconds<10) display.print('0');
    display.println(seconds);
           
  } else if (mode == 2) {
    
    display.setTextSize(1);
    dtostrf(bmp.readPressure()/100, 6, 1, stri);
    display.print(stri);
    display.print(" hPa  ");
    display.print((int) bmp.readTemperature() + TEMPMOD);
    display.println(" C");
  
    display.setCursor(0,display.height() - 7);
    dtostrf(alt - mee->_basic, 6, 1, stri);
    display.print(stri);
    display.print(" m");
  
    display.drawCircle(112, display.height()/2, 15, WHITE);
    display.drawLine(112, 1, 112, 5, WHITE);
    // 360 Grad sind 2.5m
    alt =  (2.0*PI*(alt - mee->_basic)/2.5) - PI/2.0;
    display.drawLine(
      112,
      display.height()/2,
      112                 + 13*cos(alt), 
      display.height()/2 + 13*sin(alt),
      WHITE
    );
    
    //altitude
    drawit(mee->_val, 100, mee->_nxt-1, mee->_basic);
  
    display.setCursor(58, display.height()-7);
    if (hours<10) display.print('0');
    display.print(hours);
    
    if (seconds%2 == 0)
      display.print(':');
    else
      display.print(' ');
    
    if (minutes<10) display.print('0');
    display.println(minutes);
    
  } else if (mode == 3) {
    
    display.setTextSize(2);
    display.setCursor(0,0);
    byte val = wetter(-1, 24);
    display.drawBitmap(110, 20, pic[val], 16,10, WHITE);
  }
  
  display.display();
}

