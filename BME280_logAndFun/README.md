- I add 3 buttons (to ground) A0,12,7
- a I2C display A4,A5
- the BME280 to i2c, too (has humidity)
- a jq6500 sound chip on pin 8,9 with some short sounds
- a MPU-6090 sensor on i2c to detect throwing

it displays temperature, pressure and humidity as actual value,
middle and as curve (last 60 values). It stores the values in EEPROM
every 5minutes and switch through the curves every 2 seconds.
Display off in 3 minutes. the 2 buttons are for display on and
low or high bright.

the 3. button toggles device into a toy. if you trow it
up in the air, it plays a random sound. no values are stored
in that mode.
