#include <Wire.h>
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <JQ6500_Serial.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_GFX.h>
#include <EEPROM.h>

#include "Adafruit_SSD1306.h"

#define AUDMODES_MAX   19
#define SHAKETRESHOLD   5

// HARDWARE I2C: A4 -> SDA, A5 -> SCL

#define btn1Pin  A0
#define btn2Pin  12
#define btn3Pin  7
#define randPin  A1

#define STOREAGE       60    // 60 => 5h with DAT every 5min
#define STRETCH        2     // STRETCH x STOREAGE <= 124pixel width
#define WITHDOTS       false
#define DATEVERY       300   // intervals in TICKMS miliseconds to store the next data
#define TICKMS         1000  // 1000 = 1s

#define INITEEPROM     false // "true" only the first time you want to use the device !!!!!!!

// I2C address of the MPU-6050 (SDA=PIN0 (A4), SCL=PIN2 (A5))
#define MPU6050_ADDRESS  0x68
#define BME280_ADDRESS   0x76
#define RED_LED          13

JQ6500_Serial        mp3(9, 8); // software serial Pin9 and Pin8

// --------------------------------------------------------------------------

Adafruit_SSD1306 * oled = new Adafruit_SSD1306(-1); // I2C
Adafruit_BME280 bme; // I2C

volatile uint16_t ticks = 1;
int16_t GyX,GyY,GyZ;
bool funMode = false;

void EEPROMput(int ee, int16_t value) {
  const byte* p = (const byte*)(const void*)&value;
  unsigned int i;
  for (i = 0; i < 2; i++) EEPROM.write(ee++, *p++);
}

int16_t EEPROMget(int ee) {
  int16_t value;
  byte* p = (byte*)(void*)&value;
  unsigned int i;
  for (i = 0; i < 2; i++) *p++ = EEPROM.read(ee++);
  return value;
}

struct Midways {
  short _nxt; // the eeprom address, with the index of the values
  uint16_t _max;
  uint16_t _min;

  Midways(short nxtAddr, uint16_t initval) {
    _nxt = nxtAddr;

    // only the first time ---------------------------------------------- !!!!!!!
    if (INITEEPROM == true) {
      EEPROM.write(_nxt, 0);
      for (int i=0; i<STOREAGE; ++i) { 
        EEPROMput(_nxt+1+(2*i), initval);      
      }
    }
  }

  void add(uint16_t val) {
    byte i = EEPROM.read(_nxt);
    EEPROMput(_nxt+1+(2*i), val);
    i += 1;
    if (i == STOREAGE) i = 0;
    EEPROM.write(_nxt, i);
  }

  uint16_t last() {
    short i = EEPROM.read(_nxt) -1;
    if (i < 0) i += STOREAGE;
    return EEPROMget(_nxt+1+(2*i));
  }

  uint16_t midget() {
    uint32_t mid = 0;
    uint16_t p;
    _min = 65535;
    _max = 0;
    for (byte i=0; i<STOREAGE; ++i) {
      p = EEPROMget(_nxt+1+(2*i));
      if (p > _max) _max=p;
      if (p < _min) _min=p;
      mid += p;
    }
    
    return (mid/STOREAGE);
  }

  void draw(int x, float fak) {
    int id = EEPROM.read(_nxt) -1;
    uint16_t mid = midget();
    short y = oled->height()/2;
    
    byte lastx,lasty;
    short dx = 0;
    if (id < 0) id += STOREAGE;
    short dy = y - fak*((short)EEPROMget(_nxt+1+(2*id)) - (short)mid);
    
    for (int i=0; i<STOREAGE; ++i) {
      lastx = dx;
      lasty = dy;
      
      dx = STRETCH*i;
      dy = y - fak*((short)EEPROMget(_nxt+1+(2*id)) - (short)mid);
      if (dy < 0) dy = 0;
      if (dy > (oled->height()-2)) dy = oled->height()-2;
      oled->drawLine(x+(STRETCH*(STOREAGE-1))-lastx, lasty, x+(STRETCH*(STOREAGE-1))-dx, dy, WHITE);
      if (WITHDOTS == true) oled->drawCircle(x+(STRETCH*(STOREAGE-1))-dx, dy, 2, WHITE);
      id--;
      if (id < 0) id += STOREAGE;
    }
  }
};


Midways * hpa; // pressure
Midways * hum; // huminity
Midways * temi; // temp

byte nxtAdrHpa  = 0;
byte nxtAdrHum  = nxtAdrHpa + (2*STOREAGE) + 1;
byte nxtAdrTemi = nxtAdrHum + (2*STOREAGE) + 1;

void setup() {
  Serial.begin(9600); // important on WaVGAT !!!!!!

  pinMode(btn1Pin, INPUT_PULLUP);
  pinMode(btn2Pin, INPUT_PULLUP);
  pinMode(btn3Pin, INPUT_PULLUP);
  pinMode(RED_LED, OUTPUT);

  delay(100);
  Wire.begin();
  Wire.beginTransmission(MPU6050_ADDRESS);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);

  delay(100);
  randomSeed(analogRead(randPin));

  oled->begin();
  oled->clearDisplay();
  oled->setTextColor(WHITE);
  oled->setTextSize(2);
  oled->setCursor(0, 18);
  oled->print(F("   YMO\n  Sensor"));
  oled->display();
  
  mp3.begin(9600);
  mp3.reset();
  mp3.setVolume(20);
  mp3.setLoopMode(MP3_LOOP_ONE_STOP);
  mp3.sleep();
  mp3.playFileByIndexNumber(AUDMODES_MAX);
  
  oled->setTextSize(1);

  if (!bme.begin(BME280_ADDRESS)) {  
    // Could not find a valid BME280 sensor, check wiring!
    while (1);
  }

  bme.setSampling(
    Adafruit_BME280::MODE_FORCED, // takeForcedMeasurement must be called before each reading
    Adafruit_BME280::SAMPLING_X1, // Temp. oversampling
    Adafruit_BME280::SAMPLING_X1, // Pressure oversampling
    Adafruit_BME280::SAMPLING_X1, // Humidity oversampling
    Adafruit_BME280::FILTER_OFF,
    Adafruit_BME280::STANDBY_MS_250
  );
  bme.takeForcedMeasurement();
  delay(100);
  hpa = new Midways(nxtAdrHpa, bme.readPressure()/10.0 -9000);
  hum = new Midways(nxtAdrHum, bme.readHumidity()*10);
  temi = new Midways(nxtAdrTemi, bme.readTemperature()*10 +200);
}

inline void displayClock() {
  oled->clearDisplay();
  Serial.print(bme.readPressure()/100.0);
  Serial.println(F(" hPa"));  
  Serial.print(bme.readHumidity());
  Serial.println(F(" %"));
  Serial.print(bme.readTemperature());
  Serial.println(F(" C"));    
}

void loop() {
    
  if (funMode == true) {
   // measure because we may woop
    Wire.beginTransmission(MPU6050_ADDRESS);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU6050_ADDRESS, 6, true); // we need to read 6 registers, realy!
  
    GyX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
    GyY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    GyZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  
    if (GyX < 0) GyX *= -1;
    if (GyY < 0) GyY *= -1;
    if (GyZ < 0) GyZ *= -1;
  
    if (GyX < SHAKETRESHOLD || GyY < SHAKETRESHOLD || GyZ < SHAKETRESHOLD) {
      mp3.playFileByIndexNumber(1+random(AUDMODES_MAX));
      delay(1000);
    }
    delay(20);  

  } else {
    bme.takeForcedMeasurement();
    displayClock();

    if (ticks%180 == 0) { // all 3min
      oled->ssd1306_command(SSD1306_DISPLAYOFF);      
    }
  
    if (ticks>DATEVERY) {
      hpa->add(bme.readPressure()/10.0 -9000);
      hum->add(bme.readHumidity()*10);
      temi->add(bme.readTemperature()*10 +200);
      ticks=0;
    }
    if (ticks%6 == 0 || ticks%6 == 1) {
      oled->setCursor(0, 0);
      oled->print(bme.readPressure()/100.0);
      oled->print(F(" hPa"));
      oled->setCursor(0, 56);
      oled->print(F("o"));
      oled->setCursor(0, 57);
      oled->print(F("/"));
      oled->setCursor(10, 57);
      oled->print((hpa->midget() +9000)/10.0);
      oled->print(F(" hPa"));
      hpa->draw( 2, 1.0);
    }
    if (ticks%6 == 2 || ticks%6 == 3) {
      oled->setCursor(0, 0);
      oled->print(bme.readHumidity());
      oled->print(F(" %"));
      oled->setCursor(0, 56);
      oled->print(F("o"));
      oled->setCursor(0, 57);
      oled->print(F("/"));
      oled->setCursor(10, 57);
      oled->print(hum->midget()/10.0);
      oled->print(F(" %"));
      hum->draw( 2, 2.0);
    }
    if (ticks%6 == 4 || ticks%6 == 5) {
      oled->setCursor(0, 0);
      oled->print(bme.readTemperature());
      oled->print(F(" C"));
      oled->setCursor(0, 56);
      oled->print(F("o"));
      oled->setCursor(0, 57);
      oled->print(F("/"));
      oled->setCursor(10, 57);
      oled->print((temi->midget() -200)/10.0);
      oled->print(F(" C"));
      temi->draw( 2, 4.0);
    }
    oled->setCursor(96, 57);
    byte mimi = DATEVERY-ticks - 60*((DATEVERY-ticks)/60);
    if (((DATEVERY-ticks)/60)<10) oled->print(F(" "));
    oled->print((DATEVERY-ticks)/60);
    oled->print(F(":"));
    if (mimi<10) oled->print(F("0"));
    oled->print(mimi);
    oled->display();
    delay(TICKMS);
    ticks++;
  }
  
  if (digitalRead(btn1Pin) == LOW) {
    oled->ssd1306_command(SSD1306_DISPLAYON);
    oled->dim(true);
  }
  if (digitalRead(btn3Pin) == LOW) {
    oled->ssd1306_command(SSD1306_DISPLAYON);
    oled->dim(false);
  }
  
  if (digitalRead(btn2Pin) == LOW) {
    funMode = funMode ? false : true;
    if (funMode) {
      oled->ssd1306_command(SSD1306_DISPLAYON);
      oled->clearDisplay();
      oled->drawCircle(oled->width()/2, oled->height()/2, 20, WHITE);
      oled->fillRect(0, 0, oled->width(), 2*oled->height()/3, BLACK);
      oled->drawPixel( 20, 10, WHITE);
      oled->drawPixel(103, 10, WHITE);
      oled->drawPixel( 20, 11, WHITE);
      oled->drawPixel(103, 11, WHITE);
      oled->display();
    } else {
      mp3.sleep();
    }
    delay(1000);
  }
}

