#include <MsTimer2.h>
#include <Wire.h>

/*
 * Code stores every 20min for 24h temperature and pressure.
 * It trys to make a weather forcast via 1h and 3h pressure change.
 * If forecast is unsure, it shows the the actual weather based on the
 * pressure.
 * 
 * Pressure and temperature for all 72 values is shown as graph.
 * 
 * A clock is added, too!
 */

#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#include <Adafruit_GFX.h>
#include "Adafruit_SSD1306.h"

#define MAX_POWER  4250.0
#define MIN_POWER  3600.0

#define TEMPMOD    -3.0
#define STOREAGE   72
#define STOREEVERY 20 //minutes

// battery
byte maxLength = 28;
int  vcc;

int hours    = 0;
int minutes  = 0;
int seconds  = 0;

bool dostore = false;

// -- Adjust Clock --------------------
 
#define HOURUP  10
#define MINUP    9

// ------------------------------------

Adafruit_BMP280 bmp; // I2C
Adafruit_SSD1306 display(4); // 4 is a dummy

/**
 * connect the I2C SSD1306 128x32 Display and the BMP280 Sensor
 * to SDA -> A4  and SCL -> A5
 */

struct Midways {
  float _basic;
  float _val[STOREAGE];
  int _nxt;

  Midways(float initval) {
    _nxt = 0;
    _basic = initval;
    for (int i=0; i<STOREAGE; ++i) { 
      _val[i] = _basic;
    }
  }

  void add(float val) {
    _val[_nxt] = val;
    _nxt++;
    if (_nxt == STOREAGE) {
      _nxt = 0;
    }
  }

  float last() {
    int l = _nxt -1;
    if (l < 0) l += STOREAGE;
    return _val[l];
  }

  float oneHour() {
    int l = _nxt -4;
    if (l < 0) l += STOREAGE;
    return _val[l];
  }

  float twoHours() {
    int l = _nxt -7;
    if (l < 0) l += STOREAGE;
    return _val[l];
  }
  
  float treeHours() {
    int l = _nxt -10;
    if (l < 0) l += STOREAGE;
    return _val[l];
  }

  float midget() {
    float mid = 0;
    for (int i=0; i<STOREAGE; ++i) mid += _val[i];
    
    return mid/(float)STOREAGE;
  }

  void draw(int x, float fak) {
    int id = _nxt-1;
    float mid = midget();
    int y = display.height()/2;
    if (id < 0) id += STOREAGE;
    for (int i=0; i<STOREAGE; ++i) {
      display.drawPixel(x+STOREAGE-i, y - fak*(_val[id] - mid), WHITE);
      id--;
      if (id < 0) id += STOREAGE;
    }
  }
};

Midways * hpa; // pressure
Midways * cel; // celsius
char stri[21];
int mode = 0;

static const uint8_t PROGMEM pic[6][160] = {{
B00000000,B00000000,
B00000011,B00110000,
B00000100,B11001100,
B00011000,B00000010,
B00100000,B00000010,
B00111111,B11111110,
B00000000,B00000000,
B00100100,B10001000,
B01000000,B10010000,
B01001001,B00100000
},{
B00000000,B00000000,
B00000000,B00000000,
B00000000,B00000000,
B00110000,B00000000,
B01001111,B10011000,
B10000010,B01100110,
B11111100,B00000001,
B00010000,B00000001,
B00011111,B11111111,
B00000000,B00000000
},{
B00000000,B00000000,
B00000000,B00000000,
B00000011,B00110000,
B00000100,B11001100,
B00011000,B00000010,
B00100000,B00000010,
B00111111,B11111110,
B00000000,B00000000,
B00000000,B00000000,
B00000000,B00000000
},{
B00000000,B00000000,
B00100000,B10000000,
B10010001,B00000000,
B01011010,B00000000,
B00100101,B10011000,
B11000010,B01100110,
B01001100,B00000001,
B10110000,B00000001,
B00011111,B11111111,
B00000000,B00000000
},{
B00010000,B10000000,
B00001001,B11000000,
B00000110,B00110001,
B00011000,B00001010,
B00010000,B00000100,
B01110000,B00000100,
B00011000,B00001000,
B00100110,B00110110,
B01001001,B11000001,
B00010000,B10000000
},{
B00010000,B10000000,
B01001001,B11000000,
B00100110,B00110001,
B00011000,B00001010,
B00010001,B01000100,
B01110000,B00000111,
B00011001,B11001000,
B00100100,B00010110,
B01001011,B11100001,
B00010000,B10010000
}};


void balken(int x, int y, int maxVal, int minVal, int val) {
  if (val <= minVal) {
    val = 0;
  } else if(val >= maxVal) {
    val = maxVal;
  } else {
    val = maxLength * (float)(val-minVal)/((float)(maxVal-minVal));
  }
  display.drawRect(x,   y,                 8, maxLength+2, WHITE);
  display.fillRect(x+2, y+1+maxLength-val, 4, val, WHITE);    
}

int readVcc() {
  int mv;
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(10); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
  mv = ADCL; 
  mv |= ADCH<<8; 
  mv = 1126400L / mv;
  return mv;
}






void tick() {
  seconds++;
  if (seconds%5 == 0) mode++;
  if (mode > 3) mode = 0;
  
  if (seconds >= 60) {
    minutes++; seconds = seconds%60;
    if (minutes%STOREEVERY == 0) dostore = true;
  }
  if (minutes == 60) {
    hours++;
    minutes = 0;
  }
  if (hours >= 24) {
    hours = hours%24;
  }
}



byte wetter(
  int x, int y,
  float hpa0,
  float hpa1,
  float hpa2,
  float hpa3
) {
  if ( (hpa0 - hpa1) > 1.0 ) {
    if ( (hpa1 - hpa2) > 1.0 ) {
      if ( (hpa0 - hpa3) > 4.0 ) {
        display.drawBitmap(x, y, pic[5], 16,10, WHITE);
        display.print("sun+breezy");
        return 5;
      }
      display.drawBitmap(x, y, pic[4], 16,10, WHITE);
      display.print("++ sun");
      return 4;
    } else {
      display.drawBitmap(x, y, pic[3], 16,10, WHITE);
      display.print("+ sunny");
      return 3;
    }
  }
  
  if ( (hpa0 - hpa1) < -1.0 ) {
    if ( (hpa1 - hpa2) < -1.0 ) {
      if ( (hpa0 - hpa3) < -4.0 ) {
        display.drawBitmap(x, y, pic[0], 16,10, WHITE);
        display.print("rain+storm");
        return 0;
      }
      display.drawBitmap(x, y, pic[1], 16,10, WHITE);
      display.print("++ clouds");
      return 1;
    } else {
      display.drawBitmap(x, y, pic[2], 16,10, WHITE);
      display.print("+ cloudy");
      return 2;
    }
  }

  if (hpa0 < 1009) {
    display.drawBitmap(x, y, pic[0], 16,10, WHITE);
    display.print("still rain");
    return 0;
    
  } else if (hpa0 < 1013) {
    display.drawBitmap(x, y, pic[1], 16,10, WHITE);
    display.print("still cloud");
    return 1;
    
  } else if (hpa0 < 1014.5) {
    display.drawBitmap(x, y, pic[3], 16,10, WHITE);
    display.print("unsettled");
    return 3;
    
  } else if (hpa0 < 1016) {
    display.drawBitmap(x, y, pic[4], 16,10, WHITE);
    display.print("still sunny");
    return 4;
    
  } else {
    display.drawBitmap(x, y, pic[5], 16,10, WHITE);
    display.print("still sun");
    return 5;  
  }
}



void smallClock() {
  display.setCursor(0, display.height()-7);
  if (hours<10) display.print('0');
  display.print(hours);
  
  if (seconds%2 == 0)
    display.print(':');
  else
    display.print(' ');
  
  if (minutes<10) display.print('0');
  display.println(minutes);
}




void setup() {
  pinMode(HOURUP, INPUT_PULLUP);
  pinMode(MINUP, INPUT_PULLUP);
  
  // initialize with the I2C addr 0x3C (for the 128x32)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextSize(1);
  display.setTextColor(WHITE);

  if (!bmp.begin()) {  
    // Could not find a valid BMP280 sensor, check wiring!
    while (1);
  }
  delay(100);
  hpa = new Midways(bmp.readPressure()/100.0);
  cel = new Midways(bmp.readTemperature() + TEMPMOD);
  
  for (byte i=0; i<6; ++i) {
    display.clearDisplay();
    display.drawBitmap(56, 11, pic[i], 16,10, WHITE);
    display.display();
    delay(500);
  }

  MsTimer2::set(1000, tick);
  MsTimer2::start();
}





void loop() {
  delay(200);
  
  if (digitalRead(HOURUP) == LOW) hours++;
  if (digitalRead(MINUP) == LOW)  minutes++;

  if (dostore == true) {
    hpa->add(bmp.readPressure()/100.0);
    cel->add(bmp.readTemperature() + TEMPMOD);
    dostore = false;
  }
    
  //hpa->add(bmp.readPressure()/100.0);
  //cel->add(bmp.readTemperature() + TEMPMOD);

  display.clearDisplay();
  display.setCursor(0,0);
    
  if (mode == 0) {
    display.dim(false);
      
    display.setTextSize(1);
    display.print(bmp.readTemperature() + TEMPMOD);
    display.print(" C");
    cel->draw(55, 2.0);
    smallClock();
           
  } else if (mode == 1) {
    display.dim(false);
      
    display.setTextSize(1);
    dtostrf(bmp.readPressure()/100, 6, 1, stri);
    display.print(stri);
    display.print(" hPa");
    hpa->draw(55, 4.0);
    smallClock(); 
        
  } else if (mode == 2) {
    display.dim(true);

    display.setTextSize(2);
    display.setCursor(0,0);
    wetter(
      110, 20,
      hpa->last(),
      hpa->oneHour(),
      hpa->twoHours(),
      hpa->treeHours()
    );
    display.setTextSize(1);
    smallClock(); 

  } else {
    display.dim(true);
    
    display.setTextSize(3);
    if (hours<10) display.print('0');
    display.print(hours);
    display.setCursor(45,0);
    if (minutes<10) display.print('0');
    display.print(minutes);
    display.setTextSize(2);
    display.setCursor(87,0);
    if (seconds<10) display.print('0');
    display.println(seconds);

    display.drawLine(121,0,124,0, WHITE);
    balken(119,1,MAX_POWER,MIN_POWER,readVcc());
  }
  
  display.display();
}

