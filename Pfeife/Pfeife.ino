#include <Wire.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#include <Adafruit_GFX.h>
#include "Adafruit_SSD1306.h"

#define BEEPER 11 // SPI MOSI

static const char  names[] = {'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C'};
static const short tones[] = {1915, 1700, 1519, 1432, 1275, 1136, 1014, 956};

Adafruit_BMP280 bmp; // I2C
Adafruit_SSD1306 display(4); // 4 is a dummy

float hpa1,hpa2;

void setup() {  
  pinMode(BEEPER, OUTPUT);
  // initialize with the I2C addr 0x3C (for the 128x32)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextSize(1);
  display.setTextColor(WHITE);

  if (!bmp.begin()) {  
    // Could not find a valid BMP280 sensor, check wiring!
    while (1);
  }
  delay(100);
  hpa1 = bmp.readPressure()/50.0;
  hpa2 = hpa1;
}



int id = 0;
int tick = 0;

void loop() {
  tick++;
  if (tick>60) {
    tick=0;
    hpa2 = bmp.readPressure()/50.0;
    id = (int) (hpa2 - hpa1);
    if (id <0) id *= -1;
    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(0,0);
    display.print(id);
    
    display.setCursor(0,16);
    display.print("id: ");
    id = id%8;
    display.print(id);
    display.setTextSize(3);
    display.setCursor(95, 0);
    display.print(names[id]);
    display.display();
  }
  analogWrite(BEEPER, 500);
  delayMicroseconds(tones[id]);
  analogWrite(BEEPER, 0);
  delayMicroseconds(tones[id]);
}

