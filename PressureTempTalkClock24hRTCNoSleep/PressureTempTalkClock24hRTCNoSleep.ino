#include <Arduino.h>

#include <Wire.h>
#include "DS3231M.h"

#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#include <Adafruit_GFX.h>
#include "Adafruit_SSD1306.h"
//#include "pic.h"

#define SET_INIT_RTC  0
#define TEMPMOD       -3.0
#define STOREAGE      127
#define STOREEVERY    11 //minutes

//RTCdata data = {40,53,21, 3, 21,03,18}; // (3 == )Mittwoch, 21:53:40 Uhr 21.03.2018 //7=sonntag
#if SET_INIT_RTC > 0
  RTCdata data = {0,8,18, 3, 29,03,18};
#else
  RTCdata data;
#endif

#define HOUR_BTN   7
#define MINU_BTN   8

#define MODHPA   9400.0
#define FAKTOR   4


int  vcc = 3100;
bool dostore = false;

#define STORELED  9 // unused
#define BEEPER 11   // unused    SPI MOSI

Adafruit_BMP280 bmp; // I2C
Adafruit_SSD1306 display(4); // 4 is a dummy


#include <Wtv020sd16p.h>
int resetPin = A1;  // The pin number of the reset RST
int busyPin  = A0;  // The pin number of the  busy P06
int clockPin = 12;  // The pin number of the clock P04
int dataPin  = 13;  // The pin number of the  data P05
// P02 NEXT       k1
// P03 PREV       k2
// P07 play/stop  k3
Wtv020sd16p wtv020sd16p(resetPin,clockPin,dataPin,busyPin);

bool gong = true;

enum Sounds {
  ZERO,
  EIN,ZWEI,DREI,VIER,FUENF,SECHS,SIEBEN,ACHT,NEUN,ZEHN,
  ELEFEN,TWELFE,
  ZWANZIG,DREIZIG,VIERZIG,FUENFZIG,
  OCLOCK,UND,IT_IS,EINS
};

#define DIE10 30
#define ESIST 31
#define UHR   32


void talk59(int num) {
  int tenth = num/10;
  if (num < 1) {
    wtv020sd16p.asyncPlayVoice(ZERO);
    delay(1500);

  } else if (num == 1) {
    wtv020sd16p.asyncPlayVoice(EINS);
    delay(1500);

  } else if (num > 1 && num < 13) {
    wtv020sd16p.asyncPlayVoice(num);
    delay(2500);

  } else {
    num = num - 10*tenth;
    if (num > 0) {
      wtv020sd16p.asyncPlayVoice(num);
      delay(1400);
    }
    
    if (tenth > 1 && num > 0) {
      wtv020sd16p.asyncPlayVoice(UND);
      delay(1000);
    }
    
    if (tenth == 1) {
      wtv020sd16p.asyncPlayVoice(DIE10);
      delay(1200);
    } else if (tenth == 2) {
      wtv020sd16p.asyncPlayVoice(ZWANZIG);
      delay(1500);
    } else if (tenth == 3) {
      wtv020sd16p.asyncPlayVoice(DREIZIG);
      delay(1500);
    } else if (tenth == 4) {
      wtv020sd16p.asyncPlayVoice(VIERZIG);
      delay(1500);
    } else if (tenth == 5) {
      wtv020sd16p.asyncPlayVoice(FUENFZIG);
      delay(1500);
    }
  }
}

void talkClock() {
  wtv020sd16p.asyncPlayVoice(ESIST);
  delay(2200);
  if (data.hour == 1) {
    wtv020sd16p.asyncPlayVoice(EIN);
    delay(1300);
  } else {
    talk59(data.hour);
  }
  wtv020sd16p.asyncPlayVoice(UHR);
  delay(2200);
  if (data.minute > 0) talk59(data.minute);
}

// 940.0 -> 1042.0 (+/- 0.25) 
inline byte flo2by(float val) {
  byte res = 0;
  val = val * 10;
  val = val - MODHPA;
  res = val / FAKTOR;
  return res;
}

inline float by2flo(byte val) {
  float res = val;
  res = res * FAKTOR;
  res = res + MODHPA;
  res = res / 10.0;
  return res;
}

// -25.0 to + 70.0
inline float temp2hpa(float val) {
  int q = val * 10;
  float res = map(q, -250, 700, 9400, 10420);
  return res/10.0;
}

inline float hpa2temp(float val) {
  int q = val * 10;
  float res = map(q, 9400, 10420, -250, 700);
  return res/10.0;
}

void readVcc() {
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(10); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
  vcc = ADCL; 
  vcc |= ADCH<<8; 
  vcc = 1126400L / vcc;
}

/**
 * connect the I2C SSD1306 128x32 Display and the BMP280 Sensor
 * to SDA -> A4  and SCL -> A5
 */

struct Midways {
  byte _basic;
  byte _val[STOREAGE];
  int  _nxt;
  float _max;
  float _min;

  Midways(float initval) {
    _nxt = 0;
    _basic = flo2by(initval);
    for (int i=0; i<STOREAGE; ++i) { 
      _val[i] = _basic;
    }
  }

  void add(float val) {
    _val[_nxt] = flo2by(val);
    _nxt++;
    if (_nxt == STOREAGE) {
      _nxt = 0;
    }
  }

  float last() {
    int l = _nxt -1;
    if (l < 0) l += STOREAGE;
    return by2flo(_val[l]);
  }

  float oneHour() {
    int l = _nxt -6;
    if (l < 0) l += STOREAGE;
    return by2flo(_val[l]);
  }

  float twoHours() {
    int l = _nxt -11;
    if (l < 0) l += STOREAGE;
    return by2flo(_val[l]);
  }
  
  float treeHours() {
    int l = _nxt -16;
    if (l < 0) l += STOREAGE;
    return by2flo(_val[l]);
  }

  float midget() {
    float mid = 0;
    float p;
    _min = by2flo(255);
    _max = by2flo(0);
    for (int i=0; i<STOREAGE; ++i) {
      p = by2flo(_val[i]);
      if (p > _max) _max=p;
      if (p < _min) _min=p;
      mid += p;
    }
    
    return mid/(float)STOREAGE;
  }

  void draw(int x, float fak) {
    int id = _nxt-1;
    float mid = midget();
    int y = 6 + display.height()/2;
    
    byte lastx,lasty;
    byte dx = x + STOREAGE;
    short dy = y - fak*(by2flo(_val[id]) - mid);
    
    if (id < 0) id += STOREAGE;
    for (int i=0; i<STOREAGE; ++i) {
      lastx = dx;
      lasty = dy;
      
      dx = x+STOREAGE-i;
      dy = y - fak*(by2flo(_val[id]) - mid);
      if (dy < 0)   dy = 0;
      if (dy > (display.height()-2)) dy = display.height()-2;
      display.drawLine(lastx, lasty, dx, dy, WHITE);
      id--;
      if (id < 0) id += STOREAGE;
    }
  }
};

Midways * hpa; // pressure
Midways * cel; // celsius
char stri[21];
int mode = 0;

void myClock(int x, int y) {
  display.setCursor(x, y);
  if (data.hour<10) display.print('0');
  display.print(data.hour);
  
  if (data.second%2 == 0)
    display.print(':');
  else
    display.print(' ');
  
  if (data.minute<10) display.print('0');
  display.print(data.minute);
}


void bigClock(int x, int y) {
  display.setCursor(x, y);
  if (data.hour<10) display.print('0');
  display.print(data.hour);

  if (data.second%2 == 0) {
    display.setCursor(x+49, y);
    display.print(':');
  }
    
  display.setCursor(x+68, y);
  
  if (data.minute<10) display.print('0');
  display.print(data.minute);
}


void batIcon(int x, int y) {
  display.drawRect(x, y, 19, 10, WHITE);
  display.fillRect(x+19, y+3,  2,  4, WHITE);
  if (vcc > 3130) {
    float f = (vcc - 3130)/300.0;
    if (f > 1.0) f = 1.0;
    display.fillRect(x+2, y+2,  12.0*f, 6, WHITE);
  }
}


void setup() {
  pinMode(STORELED, OUTPUT);
  pinMode(BEEPER, OUTPUT);
  pinMode(HOUR_BTN, INPUT_PULLUP);
  pinMode(MINU_BTN, INPUT_PULLUP);
  digitalWrite(STORELED, LOW);

  // initialize with the I2C addr 0x3C (for the 128x32)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextSize(1);
  display.setTextColor(WHITE);

  if (!bmp.begin()) {  
    // Could not find a valid BMP280 sensor, check wiring!
    while (1);
  }
  display.dim(true);
  wtv020sd16p.reset();
  delay(1400);
  
  display.clearDisplay();
  display.display();
  hpa = new Midways(bmp.readPressure()/100.0);
  cel = new Midways(temp2hpa(bmp.readTemperature() + TEMPMOD));

  Wire.begin();

#if SET_INIT_RTC > 0
  DS3231M_set(data);
#endif  
  delay(50);
  DS3231M_get(data);
  delay(50);
}

void loop() {
  DS3231M_get(data);
  delay(50);

  if (gong) {
    talkClock();
    DS3231M_get(data);
    gong=false;
  }
  
  // debugging
  //dostore = true;
  
  if (data.second < 5) {
    mode = 0;
  } else if (data.second < 10) {
    mode = 1;
  } else if (data.second < 15) {
    mode = 2;
  } else if (data.second < 20) {
    mode = 3;
  } else if (data.second < 25) {
    mode = 0;
  } else if (data.second < 30) {
    mode = 1;
  } else if (data.second < 35) {
    mode = 2;
  } else if (data.second < 40) {
    mode = 3;
  } else if (data.second < 45) {
    mode = 0;
  } else if (data.second < 50) {
    mode = 1;
  } else if (data.second < 55) {
    mode = 2;
  } else {
    mode = 3;
  }
  if (data.second < 2) {
    if (data.minute%15 == 0) gong = true;
    if (data.minute%STOREEVERY == 0) dostore = true;
    delay(1900);
  }



  
  if (dostore == true) {
    hpa->add( bmp.readPressure()/100.0 );
    cel->add( temp2hpa(bmp.readTemperature() + TEMPMOD) );
    dostore = false;
  }   

  display.clearDisplay();
  display.setCursor(0, 0);
  
  if (mode == 1) { // ---------------------------------------pressure
    display.setCursor(41, 0);
    display.print('/');
    display.setCursor(41, 0);
    display.print('o');
    display.setCursor(50, 0);
    display.print((int)hpa->midget());
    display.setCursor(0, 0);
    display.print((int)hpa->_min);
    display.setCursor(104, 0);
    display.print((int)hpa->_max);
    hpa->draw(0, 1.3);
    myClock(0, 25);
              
  } else if (mode == 2) { // -------------------------------temperature
    display.setCursor(41, 0);
    display.print('/');
    display.setCursor(41, 0);
    display.print('o');
    display.setCursor(50, 0);
    display.print((int)hpa2temp(cel->midget()));
    display.print(F(" C"));
    display.setCursor(0, 0);
    display.print((int)hpa2temp(cel->_min));
    display.print(F(" C"));
    display.setCursor(104, 0);
    display.print((int)hpa2temp(cel->_max));
    display.print(F(" C"));
    cel->draw(0, 2.2);
    myClock(0, 25);
    
  } else if (mode == 3) { // ------------------------------- big clock
    display.setTextSize(5);
    bigClock(0, -2);
           
  } else { // ---------------------------------------------- mode 0 = summery
    display.clearDisplay();
    display.setCursor(0, 0);
    display.setTextSize(1);
    dtostrf(bmp.readPressure()/100, 6, 1, stri);
    display.print(stri);
    display.print(F(" hPa"));

    display.setCursor(82, 0);
    dtostrf(bmp.readTemperature() + TEMPMOD, 5, 1, stri);
    display.print(stri);
    display.print(F(" C"));

    display.setCursor(0, 7);
    display.setTextSize(2);
    myClock(0, 16);
    display.setTextSize(1); 
    readVcc();
    batIcon(74, 22);
  }
      
  display.display();

  if (digitalRead(HOUR_BTN) == LOW) {
    data.hour = (data.hour+1)%24;
    DS3231M_set(data);
    delay(300);
  }
  if (digitalRead(MINU_BTN) == LOW) {
    data.minute = (data.minute+1)%60;
    DS3231M_set(data);
    delay(300);
  }

}

