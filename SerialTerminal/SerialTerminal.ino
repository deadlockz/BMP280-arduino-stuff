#include <MsTimer2.h>
#include <Wire.h>

#define UART_SPEED 9600 // 9600, 19200, 38400
#define LINES      2

#include <Adafruit_GFX.h>
#include "Adafruit_SSD1306.h"

int seconds  = 0;
int minutes  = 0;
int hours    = 0;

Adafruit_SSD1306 oled(4); // 4 is a dummy

int countX = 0;
int countY = 0;

bool printout = false;

unsigned short swipp = 0;

void scroll() {
  unsigned short oldSwipp = swipp;
  for (int i=1; i<=10; ++i) {
    oled.fillRect(0, oldSwipp, oled.width(), i, BLACK);
    oled.ssd1306_command(SSD1306_SETDISPLAYOFFSET);
    oled.ssd1306_command(swipp);
    swipp++;
    oled.display();
    delay(50);
  }
}

char umlReplace(char inChar) {
  if (inChar == -97) {
    inChar = 224; // ß
  } else if (inChar == -80) {
    inChar = 248; // °
  } else if (inChar == -67) {
    inChar = 171; // 1/2
  } else if (inChar == -78) {
    inChar = 253; // ²
  } else if (inChar == -92) {
    inChar = 132; // ä
  } else if (inChar == -74) {
    inChar = 148; // ö
  } else if (inChar == -68) {
    inChar = 129; // ü
  } else if (inChar == -124) {
    inChar = 142; // Ä
  } else if (inChar == -106) {
    inChar = 153; // Ö
  } else if (inChar == -100) {
    inChar = 154; // Ü
  } else if (inChar == -85) {
    inChar = 0xAE; // <<
  } else if (inChar == -69) {
    inChar = 0xAF; // >>
  }
  return inChar;  
}

void tick() {
  seconds++;
  printout = true;
  
  if (seconds == 60) {
    minutes++; seconds = 0;
  }
  if (minutes == 60) {
    hours++;
    minutes = 0;
  }
  if (hours == 24) {
    hours = 0;
  }
}

void setup() {
  Serial.begin(UART_SPEED);
  
  // initialize with the I2C addr 0x3C (for the 128x32)
  oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  oled.clearDisplay();
  oled.display();
  
  MsTimer2::set(1000, tick);
  MsTimer2::start();
}





void loop() {

  if (printout == true) {
    printout = false;
    
    if (hours<10) Serial.print('0');
    Serial.print(hours);
    Serial.print(':');
    if (minutes<10) Serial.print('0');
    Serial.print(minutes);
    Serial.print(':');
    if (seconds<10) Serial.print('0');
    Serial.println(seconds);
  }

  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == -61) continue; // symbol before utf-8
    if (inChar == -62) continue; // other symbol before utf-8
    if(inChar == '\n') {
      countY++;
      countX=0;
    } else {
      countX++;
      if(countX == 22) {
        countY++;
        countX=1;
      }      
    }
    inChar = umlReplace(inChar);

    if (swipp == 0) {
      if(countY>LINES) {
        countX=1;
        if(inChar == '\n') countX=0;
        countY=0;
        scroll();
      }    
    } else {
      for (int j=0;j<LINES;++j) {
        if (swipp == 10*(j+1)) {
          if(countY>j) {
            countX=1;
            if(inChar == '\n') countX=0;
            countY=j+1;
            scroll();
          } 
        }    
      }
    }
    if (swipp == 30) {
      swipp=0;
      oled.ssd1306_command(SSD1306_SETDISPLAYOFFSET);
      oled.ssd1306_command(swipp);
    }
            
    oled.setCursor((countX-1)*6,countY*10);
    oled.print(inChar);
    oled.display();
  }
}

